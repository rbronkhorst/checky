package main

import "fmt"
import "math/rand"
import "math"
import "time"

type ScoreItem struct {
	board *Board
	score float32
}

func main() {
	create_boardmap_cache()
	//deterministic seed allows us to replay things
	rand.Seed(54321)
	play()
}

func deep_score(board *Board) float32 {
	done_chan := make(chan ScoreItem)
	go deep_score1(board, 1, done_chan)
	score_item := <-done_chan
	return score_item.score
}

func deep_score1(board *Board, depth int, parent chan ScoreItem) {
	result := ScoreItem{}
	result.board = board
	if depth == 1 {
		result.score = board.score()
	} else {
		child_chan := make(chan ScoreItem)
		children := board.get_children()
		for _, c := range children {
			go deep_score1(&c, depth-1, child_chan)
		}
		max_score := float32(-1 * math.MaxFloat32)
		for range children {
			score_item := <-child_chan
			if score_item.score > max_score {
				max_score = score_item.score
			}
		}
		result.score = max_score
	}
	parent <- result
}

func turn(board *Board) *Board {
	var children []Board

	children = board.get_children()
	if len(children) > 0 {
		// choose a random one if all scores are the same
		chosen_one := children[rand.Intn(len(children))]
		chosen_score := chosen_one.score()
		for _, b := range children {
			score := deep_score(&b)
			if score > chosen_score {
				chosen_one = b
				chosen_score = score
			}
			//	fmt.Printf("Child score %v\n", score)

		}
		//fmt.Printf("Score %f, out of %d children\n", board.score(), len(children))
		return &chosen_one
	} else {
		return nil
	}
}

func play() {
	board := NewBoard()
	for {
		board = turn(board)
		if board != nil {
			board.print()
			fmt.Print(board.serialize())
			fmt.Printf("\n")
			time.Sleep(1000000000)
		} else {
			fmt.Printf("Done!\n")
			break
		}
	}
}

func test() {
	start := time.Now()
	num_runs := 1
	for i := 0; i < num_runs; i++ {
		board := NewBoard()
		for {
			board = turn(board)
			if board == nil {
				break
			}
		}
	}
	elapsed := time.Since(start)
	fmt.Printf("Total %d boards in %s\n", stats.scored_boards, elapsed)
}
