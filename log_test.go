package main

import "testing"

import "math"
import "os"
import "flag"

//import "fmt"
import "math/rand"

var index [64]uint64
var result uint64
var start_board, board Board
var children []Board

func BenchmarkChildrenCalculation(b *testing.B) {
	chain_len := 0
	total_children := 0
	resets := 0
	for i := 0; i < b.N; i++ {
		children = (&board).get_children()
		if len(children) > 0 {
			chain_len += 1
			total_children += len(children)
			chosen_one := rand.Intn(len(children))
			board = *(children[chosen_one].reverse())
			board.score()
		} else {
			//	fmt.Printf("Chain len %d\n", chain_len)
			resets += 1
			board = start_board
		}
	}
	//	avg_children := float64(total_children) / float64(chain_len)
	//	avg_chains := float64(chain_len) / float64(resets+1)
	//	fmt.Printf("Average children %f, average chains %f, tree estimation %f\n", avg_children, avg_chains, math.Pow(avg_children, float64(avg_chains)))
}

func BenchmarkLog(b *testing.B) {
	a := uint64(1) << 1
	for i := 0; i < b.N; i++ {
		result = uint64(math.Log2(float64(a)))
	}
}

func searchbit(a uint64) int {
	c := 0
	for a > 0 {
		a >>= 1
		c += 1
	}
	return c
}

func BenchmarkBitsearch(b *testing.B) {
	a := uint64(1) << 63

	for i := 0; i < b.N; i++ {
		c := 0
		d := a
		for (d & index[c]) == 0 {
			c += 1
		}
	}
}

func BenchmarkBitsearch2(b *testing.B) {
	a := uint64(1) << 63
	for i := 0; i < b.N; i++ {
		c := 0
		d := a
		for d > 0 {
			d >>= 1
			c += 1
		}
	}
}

func BenchmarkMul(b *testing.B) {
	a := uint64(512)
	c := uint64(1024)
	for i := 0; i < b.N; i++ {
		result = a * c
	}
}

func BenchmarkAnd(b *testing.B) {
	a := uint64(512)
	c := uint64(1024)
	for i := 0; i < b.N; i++ {
		result = a & c
	}
}

func BenchmarkAndNot(b *testing.B) {
	a := uint64(512)
	c := uint64(1024)
	for i := 0; i < b.N; i++ {
		result = a &^ c
	}
}

func TestMain(m *testing.M) {
	create_boardmap_cache()
	flag.Parse()

	for x := 0; x < 10; x++ {
		board.white = board.white.set(x, 9)
		board.white = board.white.set(x, 8)
		board.white = board.white.set(x, 7)
		board.white = board.white.set(x, 6)
		board.black = board.black.set(x, 0)
		board.black = board.black.set(x, 1)
		board.black = board.black.set(x, 2)
		board.black = board.black.set(x, 3)
	}
	start_board = board
	//fmt.Printf("children %d\n", len(board.get_children()))
	for i := 0; i < 64; i++ {
		index[i] = uint64(1) << uint(i)
	}
	os.Exit(m.Run())
}
