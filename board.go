package main

import "fmt"
import "math/bits"

type Board struct {
	black     Boardmap
	white     Boardmap
	black_dam Boardmap
	white_dam Boardmap
	turn      bool // true means whites turn
}

const BOARDMAP_ROWS = 10
const BOARDMAP_COLS = 5

type Boardmap uint64

type BoardmapCache struct {
	xy_to_board     [10][10]uint8
	board_to_xy     [50][2]uint8
	board_to_pos    [50]uint8
	pos             [50]Boardmap
	stars           [50]Boardmap
	hitstars        [50]Boardmap
	rays            [50]Boardmap
	corners         [50][4]Boardmap
	hotline         Boardmap
	pos_translation [4]int
	pos_offsets     [50][4][9]uint8
}

type Stats struct {
	scored_boards int
}

func dbg(format string, v ...interface{}) {
	//fmt.Printf(format, v...)
}

var bc *BoardmapCache
var stats *Stats

func create_boardmap_cache() {
	bc = new(BoardmapCache)
	stats = new(Stats)
	bc.populate_pos()
	bc.populate_xy_to_board()
	bc.populate_board_to_xy()
	bc.populate_stars()
	bc.populate_hitstars()
	bc.populate_rays()
	bc.populate_corners()
	bc.populate_pos_translation()
	bc.populate_pos_offsets()
	bc.populate_hotline()
}

func (bc *BoardmapCache) populate_hotline() {
	bc.hotline = Boardmap(31)
}

func (bc *BoardmapCache) populate_pos_translation() {
	bc.pos_translation = [4]int{-5, -4, 4, 5}
}

func (bc *BoardmapCache) populate_pos_offsets() {
	offsets := [][]int8{{-1, -1}, {1, -1}, {1, 1}, {-1, 1}}
	for pos := 0; pos < 50; pos += 1 {
		coords := bc.board_to_xy[pos]
		for corner, offsets := range offsets {
			x := int8(coords[0])
			y := int8(coords[1])
			for idx := 0; idx < 9; idx += 1 {
				x += offsets[0]
				y += offsets[1]
				if x >= 0 && x < 10 && y >= 0 && y < 10 {
					next_pos := bc.xy_to_board[x][y]
					bc.pos_offsets[pos][corner][idx] = next_pos
				} else {
					bc.pos_offsets[pos][corner][idx] = 255
				}
			}
		}
	}
}

func (bc *BoardmapCache) populate_pos() {
	for pos := 0; pos < 50; pos += 1 {
		bc.pos[pos] = Boardmap(1) << uint(pos)
	}
}
func (bc *BoardmapCache) populate_corners() {
	for pos := 0; pos < 50; pos += 1 {
		var top_left, top_right, bottom_left, bottom_right Boardmap
		coords := bc.board_to_xy[pos]
		for y := 0; y <= 9; y += 1 {
			for x := 0; x <= 9; x += 1 {
				if x < int(coords[0]) && y < int(coords[1]) {
					top_left = top_left.set(x, y)
				}
				if x > int(coords[0]) && y < int(coords[1]) {
					top_right = top_right.set(x, y)
				}
				if x > int(coords[0]) && y > int(coords[1]) {
					bottom_right = bottom_right.set(x, y)
				}
				if x < int(coords[0]) && y > int(coords[1]) {
					bottom_left = bottom_left.set(x, y)
				}

			}
		}
		bc.corners[pos][0] = top_left
		bc.corners[pos][1] = top_right
		bc.corners[pos][2] = bottom_right
		bc.corners[pos][3] = bottom_left
	}
}

func (bc *BoardmapCache) populate_hitstars() {
	offsets := [][]int8{{-2, -2}, {2, -2}, {2, 2}, {-2, 2}}
	for pos := 0; pos < 50; pos += 1 {
		coords := bc.board_to_xy[pos]
		var board Boardmap
		for _, offset := range offsets {
			x := int(coords[0]) + int(offset[0])
			y := int(coords[1]) + int(offset[1])
			if x >= 0 && x < 10 && y >= 0 && y < 10 {
				board = board.set(x, y)
			}
		}
		bc.hitstars[pos] = board
	}
}

func (bc *BoardmapCache) populate_stars() {
	offsets := [][]int8{{-1, -1}, {1, -1}, {1, 1}, {-1, 1}}
	for pos := 0; pos < 50; pos += 1 {
		coords := bc.board_to_xy[pos]
		var board Boardmap
		for _, offset := range offsets {
			x := int(coords[0]) + int(offset[0])
			y := int(coords[1]) + int(offset[1])
			if x >= 0 && x < 10 && y >= 0 && y < 10 {
				board = board.set(x, y)
			}
		}
		bc.stars[pos] = board
	}
}

func (bc *BoardmapCache) populate_rays() {
	offsets := [][]int8{{-1, -1}, {1, -1}, {1, 1}, {-1, 1}}
	for pos := 0; pos < 50; pos += 1 {
		coords := bc.board_to_xy[pos]
		var board Boardmap
		for _, offset := range offsets {
			x := int(coords[0]) + int(offset[0])
			y := int(coords[1]) + int(offset[1])
			for x >= 0 && x < 10 && y >= 0 && y < 10 {
				board = board.set(x, y)
				x += int(offset[0])
				y += int(offset[1])
			}
		}
		bc.rays[pos] = board
	}
}

func (bc *BoardmapCache) populate_xy_to_board() {
	pos := uint8(0)
	for y := 0; y < 10; y++ {
		for x := 0; x < 10; x++ {
			if y%2 == x%2 {
				bc.xy_to_board[x][y] = 255
			} else {
				bc.xy_to_board[x][y] = pos
				pos += 1
			}
		}
	}
}

func (bc *BoardmapCache) populate_board_to_xy() {
	for pos := uint8(0); pos < 50; pos += 1 {
		y := pos / 5
		x := (pos % 5) * 2
		if y%2 == 0 {
			x += 1
		}
		bc.board_to_xy[pos][0] = uint8(x)
		bc.board_to_xy[pos][1] = uint8(y)
	}
}

func (b Boardmap) get(x int, y int) uint8 {
	pos := bc.xy_to_board[x][y]
	if pos == 255 {
		return 255
	}
	return uint8(b>>pos) & 1
}

func (b Boardmap) set(x int, y int) Boardmap {
	pos := bc.xy_to_board[x][y]
	if pos == 255 {
		return b
	}
	return b | Boardmap(1)<<pos
}

func (b Boardmap) unset(x int, y int) Boardmap {
	pos := bc.xy_to_board[x][y]
	return b &^ (Boardmap(1) << pos)
}

func (b Boardmap) reverse() Boardmap {
	var r Boardmap
	for ctr := 0; ctr < 50; ctr += 1 {
		r <<= 1
		r |= b & 1
		b >>= 1
	}
	return r
}

func (b Boardmap) print() {
	for y := 0; y < 10; y++ {
		row := ""
		for x := 0; x < 10; x++ {
			value := b.get(x, y)
			if value == 1 {
				row += " X"
			} else {
				row += " ."
			}
		}
		fmt.Println(row)
	}
}
func NewBoard() *Board {
	board := new(Board)
	for x := 0; x < 10; x++ {
		board.white = board.white.set(x, 9)
		board.white = board.white.set(x, 8)
		board.white = board.white.set(x, 7)
		board.white = board.white.set(x, 6)
		board.black = board.black.set(x, 0)
		board.black = board.black.set(x, 1)
		board.black = board.black.set(x, 2)
		board.black = board.black.set(x, 3)
	}
	board.turn = true
	return board
}

func (b *Board) reverse() *Board {
	r := new(Board)
	r.black = b.white.reverse()
	r.white = b.black.reverse()
	r.black_dam = b.white_dam.reverse()
	r.white_dam = b.black_dam.reverse()
	r.turn = !b.turn
	return r
}

func (b *Board) spawn() Board {
	board := *b
	if board.turn {
		board.turn = false
	} else {
		board.turn = true
	}
	return board
}

func (b *Board) print() {
	for y := 0; y < 10; y++ {
		row := ""
		for x := 0; x < 10; x++ {
			ws := b.white.get(x, y)
			bs := b.black.get(x, y)
			wd := b.white_dam.get(x, y)
			bd := b.black_dam.get(x, y)
			if ws == 1 {
				row += " x"
			} else if bs == 1 {
				row += " o"
			} else if bd == 1 {
				row += " O"
			} else if wd == 1 {
				row += " X"
			} else {
				row += " ."
			}
		}
		fmt.Println(row)
	}
}

func (b *Board) serialize() string {
	return fmt.Sprintf("%X,%X,%X,%X,%t", b.black, b.white, b.black_dam, b.white_dam, b.turn)
}

func remove_duplicates(elements []Board) []Board {
	// Use map to record duplicates as we find them.
	encountered := map[Board]bool{}
	result := []Board{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

func (b *Board) get_dam_hit_depth(pos uint8, occupied Boardmap) ([]Board, uint) {
	var board Board
	var biggest_depth uint
	dbg("< -- Getting damhits for %d\n", pos)
	board = *b
	pos_boardmap := bc.pos[pos]
	result_boards := make([]Board, 0, 5)
	result_boards = append(result_boards, *b)
	blacks := b.black | b.black_dam
	whites := b.white | b.white_dam
	for corner_idx := 0; corner_idx < 4; corner_idx += 1 {
		dbg("Pos %d checking corner %d\n", pos, corner_idx)
		corner := bc.corners[pos][corner_idx]
		victim := Boardmap(0)

		if bc.rays[pos]&corner&blacks == 0 {
			// no victims to hit
			continue
		}
		for _, land_pos := range bc.pos_offsets[pos][corner_idx] {
			dbg("Pos %d checking landpos %d in corner %d\n", pos, land_pos, corner_idx)
			if land_pos == 255 || bc.pos[land_pos]&whites > 0 {
				// hit the edge of the board, or a white piece
				break
			}

			land_pos_board := bc.pos[land_pos]
			if victim == 0 {
				if land_pos_board&blacks > 0 {
					dbg("Victim found on pos %d\n", land_pos)
					victim = land_pos_board
				}
				continue
			} else {
				if land_pos_board&occupied > 0 {
					break
				}
			}

			move := land_pos_board &^ occupied

			if move > 0 && victim != 0 {
				board = b.spawn()
				board.white_dam = (board.white_dam | move) &^ pos_boardmap
				board.black &^= victim
				board.black_dam &^= victim
				dbg("Pos %d found dam landing site %d in corner %d\n", pos, land_pos, corner_idx)
				win_board, depth := board.get_dam_hit_depth(land_pos, occupied)
				if depth+1 == biggest_depth {
					result_boards = append(result_boards, win_board...)
				}
				if depth+1 > biggest_depth {
					biggest_depth = depth + 1
					result_boards = make([]Board, 0, 5)
					result_boards = append(result_boards, win_board...)
				}

			}
		}
	}
	dbg("Removing dupes")
	result_boards = remove_duplicates(result_boards)

	dbg(">-- Returning %d hits for %d depth %d\n", len(result_boards), pos, biggest_depth)
	return result_boards, biggest_depth
}

func (b *Board) get_dam_hits() []Board {
	result := make([]Board, 0, 5)
	occupied := b.black | b.white | b.white_dam | b.black_dam
	var biggest_depth uint
	for pos := uint8(0); pos < 50; pos += 1 {
		pos_boardmap := bc.pos[pos]
		if b.white_dam&pos_boardmap == 0 {
			continue
		}
		win_boards, depth := b.get_dam_hit_depth(pos, occupied)
		if depth == biggest_depth && depth > 0 {
			dbg("Adding %d damhit boards to current depth level result array %d\n", len(win_boards), depth)
			result = append(result, win_boards...)
		}
		if depth > biggest_depth {
			dbg("Depth level up, creating new result array at level %d with %d damhit boards\n", depth, len(win_boards))
			biggest_depth = depth
			result = win_boards
		}

		if depth > 0 {
			dbg("Got depth %d damhit moves for pos %d\n", depth, pos)
			//result[0].print()
		}
	}
	dbg("Returning %d damhit moves\n", len(result))
	return result
}

func (b *Board) get_hit_depth(pos uint8, occupied Boardmap) ([]Board, uint) {
	var board Board
	var biggest_depth uint
	dbg("Getting hits for %d\n", pos)
	board = *b
	pos_boardmap := bc.pos[pos]
	result_boards := make([]Board, 0, 5)
	result_boards = append(result_boards, *b)

	for corner_idx := 0; corner_idx < 4; corner_idx += 1 {
		dbg("Pos %d checking corner %d\n", pos, corner_idx)
		corner := bc.corners[pos][corner_idx]
		move := bc.hitstars[pos] & corner &^ occupied
		victim := bc.stars[pos] & corner & (b.black | b.black_dam)

		if move > 0 && victim > 0 {
			board = b.spawn()
			if move&bc.hotline > 0 {
				board.white_dam |= move
				board.white &^= pos_boardmap
			} else {
				board.white = (board.white | move) &^ pos_boardmap
			}
			board.black &^= victim
			board.black_dam &^= victim
			new_pos := bc.pos_offsets[pos][corner_idx][1]
			dbg("Pos %d found pos %d in corner %d\n", pos, new_pos, corner_idx)
			win_board, depth := board.get_hit_depth(new_pos, occupied)
			if depth+1 == biggest_depth {
				result_boards = append(result_boards, win_board...)
			}
			if depth+1 > biggest_depth {
				biggest_depth = depth + 1
				result_boards = make([]Board, 0, 5)
				result_boards = append(result_boards, win_board...)
			}

		}
	}
	dbg("Returning %d hits for %d depth %d\n", len(result_boards), pos, biggest_depth)
	return result_boards, biggest_depth
}

func (b *Board) get_hits() []Board {
	result := make([]Board, 0, 5)
	occupied := b.black | b.white | b.white_dam | b.black_dam
	var biggest_depth uint
	for pos := uint8(0); pos < 50; pos += 1 {
		pos_boardmap := bc.pos[pos]
		if b.white&pos_boardmap != 0 {
			win_boards, depth := b.get_hit_depth(pos, occupied)
			if depth == biggest_depth && depth > 0 {
				dbg("Adding %d hit boards to current depth level result array %d\n", len(win_boards), depth)
				result = append(result, win_boards...)
			}
			if depth > biggest_depth {
				dbg("Depth level up, creating new result array at level %d with %d hit boards\n", depth, len(win_boards))
				biggest_depth = depth
				result = make([]Board, 0, 5)
				result = append(result, win_boards...)
			}
			if depth > 0 {
				dbg("Got depth %d hit moves for pos %d\n", depth, pos)
				//result[0].print()
			}
		}
		if b.white_dam&pos_boardmap != 0 {
			win_boards, depth := b.get_dam_hit_depth(pos, occupied)
			if depth == biggest_depth && depth > 0 {
				dbg("Adding %d damhit boards to current depth level result array %d\n", len(win_boards), depth)
				result = append(result, win_boards...)
			}
			if depth > biggest_depth {
				dbg("Depth level up, creating new result array at level %d with %d damhit boards\n", depth, len(win_boards))
				biggest_depth = depth
				result = make([]Board, 0, 5)
				result = append(result, win_boards...)
			}
			if depth > 0 {
				dbg("Got depth %d damhit moves for pos %d\n", depth, pos)
				//result[0].print()
			}
		}

	}
	dbg("Returning %d hitmoves\n", len(result))
	return result
}

func (b *Board) get_dam_moves(pos uint, pos_boardmap Boardmap, occupied Boardmap) []Board {
	var board Board
	result := make([]Board, 0, 10)
	for corner := 0; corner < 4; corner += 1 {
		for _, land_pos := range bc.pos_offsets[pos][corner] {
			if land_pos == 255 {
				break
			}
			move := bc.pos[land_pos] &^ occupied
			if move > 0 {
				board = b.spawn()
				board.white_dam = (board.white_dam | move) &^ pos_boardmap
				result = append(result, board)
			} else {
				break
			}
		}
	}
	return result
}

func (b *Board) get_simple_moves(pos uint, pos_boardmap Boardmap, occupied Boardmap) []Board {
	var board Board
	result := make([]Board, 0, 5)
	for corner := 0; corner < 2; corner += 1 {
		move := bc.stars[pos] & bc.corners[pos][corner] &^ occupied
		if move > 0 {
			board = b.spawn()
			if move&bc.hotline > 0 {
				// dam upgrade!
				board.white_dam = board.white_dam | move
				board.white = board.white &^ pos_boardmap
			} else {
				// normal move
				board.white = (board.white | move) &^ pos_boardmap
			}
			result = append(result, board)
		}
	}
	return result
}

// very simple and basic way of giving a score to a board
// positive value: current player most likely wins,
// negative value: opponent most likely wins,
// 0: unknown
func (b *Board) score() float32 {
	ws := bits.OnesCount64(uint64(b.white))
	bs := bits.OnesCount64(uint64(b.black))
	simples := (float32(ws-bs) / 20.0) * float32(0.2)
	dbg("white %d, black %d result %v\n", ws, bs, simples)
	wd := bits.OnesCount64(uint64(b.white_dam))
	bd := bits.OnesCount64(uint64(b.black_dam))
	dams := (float32(wd-bd) / 20.0) * float32(0.5)
	score := simples + dams
	if bd+bs == 0 {
		score = 1
	}
	if wd+ws == 0 {
		score = 0
	}
	if b.turn == false {
		score = -1 * score
	}
	stats.scored_boards += 1
	return score
}

func (b *Board) get_moves() []Board {
	result := make([]Board, 0, 5)
	occupied := b.black | b.white | b.white_dam | b.black_dam
	for pos := uint(0); pos < 50; pos += 1 {
		pos_boardmap := bc.pos[pos]
		if b.white&pos_boardmap != 0 {
			pos_moves := b.get_simple_moves(pos, pos_boardmap, occupied)
			dbg("Got %d simple moves for pos %d\n", len(pos_moves), pos)
			result = append(result, pos_moves...)
		}
		if b.white_dam&pos_boardmap != 0 {
			pos_dam_moves := b.get_dam_moves(pos, pos_boardmap, occupied)
			dbg("Got %d dam moves for pos %d\n", len(pos_dam_moves), pos)
			result = append(result, pos_dam_moves...)
		}

	}
	dbg("Total %d possible moves\n", len(result))
	return result
}

// gets all allowed moves for this board.
func (b *Board) get_children() []Board {
	// the rest of the functions assume that it is whites turn.
	// if not, reverse the board
	was_turn := b.turn
	if b.turn == false {
		b = b.reverse()
	}
	result := b.get_hits()
	if len(result) == 0 {
		result = b.get_moves()
	}

	if was_turn == false {
		for i, _ := range result {
			result[i] = *(result[i].reverse())
		}
	}
	return result
}
